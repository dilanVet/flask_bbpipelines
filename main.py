
from flask import Flask
import os


application = Flask(__name__)

@application.route("/")
def hello_world():
    output = os.environ['APP_CODE']
    return "<p>Hello, World " + output +"8!</p>"

if __name__ == "__main__":
    application.run()